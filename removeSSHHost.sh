if [ "$1" != ""  ]
then
	sed -i -e "s/$1 .*//g" ~/.ssh/known_hosts
else
	sed -i -e "s/^172\.16\.0\.1 .*//g" ~/.ssh/known_hosts
fi
sed -i -e "/^$/d" ~/.ssh/known_hosts
exit $? 
