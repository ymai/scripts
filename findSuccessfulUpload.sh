#/bin/bash
set -eui

zgrep -ri "INFO  Upload -" ./ | grep -A3 "Posting data.*(01)1038065\d\{7\}(10)$1" | grep "Posting data to server is successful" | wc -l
