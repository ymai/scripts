set -u
echo recording $1 times in $2
mkdir -p $2
for ((i=0; i<$1; i++))
do
	rpicam-vid -c config.txt -o $2/clarios_line2_$i.h264
done
