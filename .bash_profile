#! /bin/bash
alias ls="ls -G"
alias ll="ls -GFlash"
alias cp="cp -i"
#alias vlc="/Applications/VLC.app/Contents/MacOS/VLC"
alias diff="diff -Bwsiy"
functions java17()
{
    JAVA_HOME=$JAVA_HOME17
    PATH=$JAVA_HOME17/bin:$PATH
}

if [ -f ~/.bash_env ]; then
	source ~/.bash_env
fi
#OLDMAC export PATH=$PATH:~/doc/adt-bundle-mac-x86_64/sdk/tools/:~/doc/adt-bundle-mac-x86_64/sdk/platform-tools/:/usr/local/mysql/bin/:"/Users/yusufmai/Box Sync/CN-IBM Shared Folder/CN images/Scripts/"
#export PATH=$PATH:/Users/yusufmai/Work/RAIVR/Scripts/

##
# Your previous /Users/yusufmai/.bash_profile file was backed up as /Users/yusufmai/.bash_profile.macports-saved_2013-02-21_at_10:04:25
##

# MacPorts Installer addition on 2013-02-21_at_10:04:25: adding an appropriate PATH variable for use with MacPorts.
export PATH=~/0users/AppDev/Scripts:/opt/local/bin:/opt/local/sbin:$PATH
export PATH=$M2_HOME/bin:$PATH
export PATH="$VSCODE_HOME/bin":$PATH
export PATH=$PATH:$HOME/.dotnet/tools
export PATH=$PATH:$JAVA_HOME/bin
# Finished adapting your PATH environment variable for use with MacPorts.
# modified from /etc/bashrc
#OLDMAC export PATH=$POSTGRE_HOME/bin:$JXCORE_HOME:$MONGO_HOME/bin:$M2_HOME/bin:$NODE_HOME/bin:$ARM_CS_TOOLS_HOME/bin:$GROOVY_HOME/bin:$ANDROID_HOME/platform-tools:$PATH
#PS1='\h:\W \u\$ '
export PS1='\w\>'

export NVM_DIR="/Users/yusufmai/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
export PATH=$CFPATH:$PATH
export PATH=$PYTHON_PATH:$PATH
export PATH=$DSBULK_HOME/bin/:$PATH
export PATH=$HOME/0users/AppDev/Scripts/OSX:$PATH

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
# __conda_setup="$('/Users/yusufmai/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
# if [ $? -eq 0 ]; then
#     eval "$__conda_setup"
# else
#     if [ -f "/Users/yusufmai/anaconda3/etc/profile.d/conda.sh" ]; then
#         . "/Users/yusufmai/anaconda3/etc/profile.d/conda.sh"
#     else
#         export PATH="/Users/yusufmai/anaconda3/bin:$PATH"
#     fi
# fi
# unset __conda_setup
# <<< conda initialize <<<

alias ic="ibmcloud"
export HISTFILESIZE=1000000000
export HISTSIZE=1000000
export HISTTIMEFORMAT="%m/%d/%y %T "
echo "RUNNING BASHRC"
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init --path)"
eval "$(pyenv init -)"

