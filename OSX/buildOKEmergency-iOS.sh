#/bin/sh
VERSION=`date "+%Y%m%d"`
TARGET=Release
BUILD_DIR=$PWD
APP="OKEmergency"
APPNAME=OK\ Emergency\ App
APPPATH=../Source/trunk/iPhone/OKEmergency
ENTITLEMENT=""
MOBILEPROVISION="OSEmergencyAdhoc2015"

function End 
{
        cd $BUILD_DIR
        if [ -d bin ]; then
                rm -rf bin 
        fi  
	tmpfilecnt=`ls *.tmp 2> /dev/null | wc -w`
	if [ $tmpfilecnt != 0 ]; then
        	rm *.tmp
	fi
        exit $1
}

function CheckFileSize
{
        filesizet=$(du -h $1)
        filesize=($filesizet#B)
        echo $filesize;
}

while getopts e:v:t:a:p: opt;
	do
	case $opt in
	v)
			VERSION="v$OPTARG"
			echo "Specified building for version $VERSION" >&2
			;;
	t)
			TARGET="$OPTARG"
			echo "Specified target: $TARGET" >&2 
			;;
	a)
			APP="$OPTARG"
			echo "Specified app: $APP" >&2
			;;
	p)
			APPPATH="$OPTARG"
			echo "Specified app path: $APPPATH" >&2
			;;
    	e)  
			ENTITLEMENT="$OPTARG"
        	    	echo "Specified building with entitlement: $ENTITLEMENT" >&2
            		;;  
	#check to see if there is any option that is not recognized here (e.g. -k, which is not part of the getopts list of valid options)
	\?)
			echo "Invalid option: -$OPTARG" >&2 
			usage;
			exit 1
			;;  
	#check to see if there is any valid options that expecting an argument but not there
	:)
			echo "Option -$OPTARG requires an argument." >&2 
			usage
			exit 1
			;;
	esac
	done
shift $(($OPTIND-1))

if [ "$APP" == "" ]; then
	echo "---------USAGE------------"
	echo "Please specify Application to be built: $0 <-a appname> [-p apppath] [-v version] [-t target] [-e entitlement]"
	echo "-  -a appname has to be provided."
	echo "-  -p app path will be the relative path from the current location to the directory where the" 
	echo "  .xcodeproj file is.  If not provided, App name will be put into"
	echo "  ../Source/trunk/iPhone/CoSD_SDK7, which is the directory set up in SVN."
	echo "- If version is not provided, it will use today's date as version"
	echo "- If target is not provided, it will use Release as target"
	echo "--------------------------"
	exit 1
fi

# The only remaining case is $APPPATH is not null, so script will just use that.

if [ "$ENTITLEMENT" != ""  ]; then
	if [ ! -f $ENTITLEMENT  ]; then
		echo "Entitlement file $ENTITLEMENT not found"
		End 5 
	fi
fi

echo "----Building $APP $VERSION for $TARGET----"
#KS=$(sec default-keychain)
security find-certificate -c iPhone\ Distribution:\ $APPNAME > tpem.tmp
FS=$(CheckFileSize tpem.tmp)
if [ "$FS" == '0B' ]; then
        if [ -f $MOBILEPROVISION.p12 ]; then
                echo "------Import started------"
                security add-certificate $MOBILEPROVISION.cer
                security import $MOBILEPROVISION.p12 -f pkcs12
                #security add-certificate AppleWWDRCA.cer
                echo "------Import completed------"
        else
                echo "Error: Please obtain files"
                End 4
        fi  
fi

SECCNT=$(security find-identity -v | grep $APPNAME | wc -l)
if [ "$SECCNT" -lt "1" ]; then
	echo "***** Attention *****"
        echo "$SECCNT certificates found for $APPNAME, please select the certifcate that"
        echo "should be used for signing this app by entering the number shown at the beginning of the lines." 
        security find-identity -v | grep $APPNAME > Seccertlist.tmp
	cat Seccertlist.tmp
	read -p "Please select from above list > " SECINDEX
        SECID=`security find-identity -v | grep $APPNAME | grep $SECINDEX | awk '{print $2}'`
	rm Seccertlist.tmp
elif [ $SECCNT == 1 ]; then
	SECID=`security find-identity -v | grep $APPNAME | awk '{print $2}'`
fi
echo Selected certificate is $SECID
echo

echo "----Compiling code at $PWD----"
cd $APPPATH
/usr/bin/xcodebuild -alltargets clean  > /dev/null 2>&1
if [ $? -gt 0 ]; then
    echo "****Error occured in cleaning.****" >&2
    End 1
fi

/usr/bin/xcodebuild -project $APP.xcodeproj -target $APP CODE_SIGN_IDENTITY="" CODE_SIGNING_REQUIRED=NO -configuration $TARGET -verbose > /dev/null 1>&1 
if [ $? -gt 0 ]; then
    echo "****Error occured in compiling.****" >&2
    End 1
fi

echo "----Archiving $APP version $VERSION for $TARGET----"
mkdir -p $BUILD_DIR/iOS/$VERSION/$TARGET

cp -rf "build/$TARGET-iphoneos/$APP.app" $BUILD_DIR/iOS/$VERSION/$TARGET/
cd $BUILD_DIR/iOS/$VERSION/$TARGET
appresign $APP.app $SECID -p $BUILD_DIR/$MOBILEPROVISION.mobileprovision -e $BUILD_DIR/$ENTITLEMENT  $APP\_$TARGET\_$VERSION.ipa #> /dev/null 2>&1
#rm -f $APP-1.ipa
End 0
