DECLARE -i INDEX
INDEX=1
while :; do
	INDEX=$((1 - $INDEX))
	printf "$INDEX "
	netstat -an | grep "\.$1\ " | grep -i "LISTEN\|ESTABLISHED"; 
	sleep 1
done
