minikube start  --driver=hyperkit --container-runtime=docker
minikube mount $HOME:/host
sudo /usr/local/sbin/named
minikube kubectl get nodes
kubectl describe pod/ingress-nginx-controller-xxxxxx --namespace=kube-system
kubectl describe po -A
minikube ssh
eval $(minikube docker-env)
minikube addons enable ingress1
# docker ps size
docker ps -a --size --format "table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.Size}}"
docker images ls
docker images prune -a
docker system df -v
docker system prune -a #(prune all non-running images)

#enable buildx for docker
#download from https://github.com/docker/buildx/releases/tag/v0.9.1 to $HOME/.docker/cli-plugins
docker context create ctx-buildx
docker buildx create ctx-buildx --use
docker buildx ls
# install all the platform emulator
docker run -it --rm --privileged tonistiigi/binfmt --install all
docker buildx inspect #to check what platform emulator is installed
#after restart of minikube need to remove context and re-enable all platform 
docker context rm ctx-buildx
docker buildx prune

#docker buildx sample 
 declare IMAGE=quay.io/iotvision/uc-deployment-vehicle
 declare VERSION=latest
#docker buildx build --platform linux/amd64,linux/ppc64le,linux/arm64,linux/386,linux/arm/v7,linux/arm/v6 -t $IMAGE:$VERSION --push . 
 docker buildx build --platform linux/amd64,linux/ppc64le -t  $IMAGE:$VERSION --push .
 docker buildx imagetools inspect $IMAGE:$VERSION

# {
#   "supported": [
#     "linux/amd64",
#     "linux/arm64",
#     "linux/riscv64",
#     "linux/ppc64le",
#     "linux/s390x",
#     "linux/386",
#     "linux/mips64le",
#     "linux/mips64",
#     "linux/arm/v7",
#     "linux/arm/v6"
#   ],
#   "emulators": [
#     "qemu-aarch64",
#     "qemu-arm",
#     "qemu-mips64",
#     "qemu-mips64el",
#     "qemu-ppc64le",
#     "qemu-riscv64",
#     "qemu-s390x"
#   ]
# }
#

# You can run "kubectl get all -n iot-edge" to see a list of what's running now.
# 
# When you make a change to the config, you apply it to the cluster with "kubectl apply -f crackdetection.yml"
# 
# You delete with "kubectl delete -f crackdetection.yml"
# 
# You can see the status of something by running "kubectl describe <type> -n iot-edge <name>". e.g. "kubectl describe pod -n iot-edge crackdetection"
# 
# You can get logs from the pod by running "kubectl logs -n iot-edge <name>"
# 
# thanks  doing all these now.  So not need for any tansu specific command then
# 
# Finally, if you need a shell into the container, "kubectl exec --stdin --tty -n iot-edge crackdetection -- /bin/bash"

 
