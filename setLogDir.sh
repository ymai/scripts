#/bin/sh
function setLOGDIR
{
	if [ "$LOGDIR" == "" ]; then
		echo "Please enter the directory where log should be output to."
		#read -p "Please enter the directory where log should be output to.";  tmpLOGDIR
		read tmpLOGDIR;
	else
		echo "Log is stored in $LOGDIR"
	fi
}

setLOGDIR
while [ "$tmpLOGDIR" != "" ];
do 
	echo "Setting LOGDIR to $tmpLOGDIR [yn]"
	#stty raw -echo ; answer=$(head -c 1) ; stty sane # Care playing with stty
	read answer
	if echo "$answer" | grep -iq "^y" ;then
		echo echo export LOGDIR=$tmpLOGDIR > tmpENV.sh
		chmod 755 tmpENV.sh
		eval $(./tmpENV.sh) #handle ~
		rm tmpENV.sh 
		break;
	else
		echo 
		setLOGDIR
	fi
done
echo Logging to $LOGDIR
