#!/usr/bin/python

import cv2
import os

inputdir = '.'
outputdir = 'eqImages'
for file in os.listdir(inputdir):
    img = cv2.imread(inputdir+'/'+file,0)
    eq_img = cv2.equalizeHist(img)
    cv2.imwrite(outputdir+'/'+file, eq_img)
