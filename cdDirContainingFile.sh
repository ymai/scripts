#!/bin/bash

#if [ "$0" != "-bash" ] && [ "$0" != "-sh" ]; then
#	echo "Please execute by 'source cdDirContainingFile.sh <file>'"
#	exit 1;
#fi

if [ $# -eq 0 ]; then
	echo "Please provide file name to search for"
	exit 2;
fi

echo cd to directory containing $1
folder=$(find . -iname $(basename $1))
echo $1 is found in $folder
IFS1="$IFS"
SEPARATOR="/"
IFS=$SEPARATOR eval 'paths=($folder)'
#read -r -a paths <<< $folder
fullpath=""
#for path in "${paths[@]}"
#do
#	echo "--> $path"
#	i++;
#	fullpath=$fullpath+"/"+$path
#done
pathslen=${#paths[@]}
for (( i=0; i<${pathslen}-1; i++));
do
	path=${paths[$i]}
	if [ $i -eq 0 ]; then
		fullpath=$path
	else
		fullpath=$fullpath$SEPARATOR$path
	fi
done
IFS="$IFS1"

echo cd to $fullpath
cd $fullpath
