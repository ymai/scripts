function startScreen
{
        screen -S $1 -dm bash -c "cd $2; $3; exec bash --rcfile <(echo '. ~/.bashrc; PS1=\${STY}:\$PS1')"
}
# startScreen AldoCropper "/home/paperspace/AldoCropper" "python server.py"
# startScreen "All.Server" "/home/paperspace/custom-faster-rcnn/tools" "python server.py"
# startScreen "All.Training" "/home/paperspace/custom-faster-rcnn" "python worker.py"
# startScreen "RAIVR.prediction" "/home/paperspace/bolt-faster-rcnn/tools" "python server.py"
