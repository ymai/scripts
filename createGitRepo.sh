if [ $# -ne 1 ]
then
        echo "Please provide git repo name"
        exit 99
fi
pushd /opt/git/
sudo git init --bare --shared=group $1
sudo chgrp -R git $1/
cd $1
sudo git config core.sharedRepository group
popd
