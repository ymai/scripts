function debug
{
	local PARAMS
	local PARAM0TMP
	local PARAM0
	local PARAMARR
	local PARAM
	local -i INDEX
	local OPTIONS
	local VAR=""
	local OUTPUTFILE=$DEBUGFILE
	local FORCE_STDOUT=0
	for VAR in "$@"
	do
		if [ "${VAR:0:1}" = "-" ]
		then
			if [ "${VAR:0:2}" = "-f" ] && [ "${#VAR}" -gt 2 ]
			then
				OUTPUTFILE=${VAR:2}
			elif [ "${VAR:0:2}" = "-s" ]
			then
				FORCE_STDOUT=1
			else
				OPTIONS=$OPTIONS" "$VAR
			fi
		else
			if [ "$PARAM0TMP" = "" ]
			then
				PARAM0TMP=$PARAMS
				PARAMS=$VAR
			else
				PARAMS=$PARAMS" "$VAR
			fi

		fi

	done
	OLDIFS=$IFS
	IFS=$'\n' PARAMARR=($PARAMS)

	if [ ${#PARAMARR} -gt 1 ] && [[ $PARAM0TMP =~ ^(\\[a-z])+$ ]]
	then
		PARAM0=$PARAM0TMP
	fi

	for PARAM in ${PARAMARR[*]}
	do
		IFS=$OLDIFS
		if [ ${#OUTPUTFILE} -eq 0 ] || [ $FORCE_STDOUT -eq 1 ]
		then
			echo $OPTIONS "<"`date "+%Y/%m/%d %T"`"> $PARAM0 $PARAM"
		fi
		if [ ${#OUTPUTFILE} -gt 0 ]
		then
			echo $OPTIONS "<"`date "+%Y/%m/%d %T"`"> $PARAM0 $PARAM" >> $OUTPUTFILE
		fi
		let INDEX+=1
	done
}
