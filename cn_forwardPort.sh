#!/bin/bash

# note: local may not work on some cases for port forwarding to non-master nodes
# since some services need to go to `iotvision.master` instead of `iotvision.node`
user="iotvision"
local="iotvision.node"
remote=""

case $1 in
    -d7 | --development7)
        remote="192.168.102.7"
        ;;
    -d8 | --development8)
        remote="192.168.102.8"
        ;;
    -g1 | --staging-gbs-1)
	remote="gbs1.cn.ca"
	;;
    -g2 | --staging-gbs-2)
	remote="gbs2.cn.ca"
	;;
    -s3 | --staging3)
        remote="192.168.102.125"
        ;;
    -s2 | --staging2)
        remote="192.168.102.126"
        ;;
    -r1 | --rivers1)
        remote="10.221.9.42"
        ;;
    -r2 | --rivers2)
        remote="10.221.9.45"
        ;;
    -r3 | --rivers3)
        remote="10.221.9.44"
        ;;
    -r4 | --rivers4)
        remote="10.221.9.43"
        ;;
    -y1 | --yazoo1)
	remote="10.221.11.30"
    	;;
    *)
        user="$1"
        local="$2"
        remote="$3"
        ;;
esac
set -u
echo "Connecting to $user@$remote"
ssh \
-L 9042:$local:9042 \
-L 15672:$local:15672 \
-L 5601:$local:5601 \
-L 6479:$local:6479 \
-L 8902:$local:8902 \
-L 8906:$local:8906 \
-L 9000:$local:9000 \
-L 9001:$local:9001 \
-L 4242:$local:4242 \
-L 9090:$local:9090 \
-L 3000:$local:3000 \
-L 8900:$local:8989 \
-N $user@$remote
# if [ $# -eq 2 ]
# then
# IP=$1
# IP2=$2
# echo "ssh \
#  -L 15672:$IP:15672 \
#  -L 5672:$IP:5672 \
#  -L 8906:$IP:8906 \
#  -L 9042:$IP:9042 \
#  -L 5601:$IP:5601 \
#  -L 9001:$IP:9001 \
#  -L 8989:$IP:8989 \
#  -L 3000:$IP:3000 \
#  -N iotvision@$IP2"
#  exit 0
# fi
# echo ssh -L 15672:10.221.9.$1:15672 -L 5672:192.168.101.$2:5672  -L 8906:10.221.9.$1:8906 -L 9042:192.168.101.$2:9042   -L 5601:192.168.101.$2:5601  -L 9001:192.168.101.$2:9001  -L 8989:192.168.101.$2:8989 -L 3000:192.168.101.$2:3000 -N iotvision@iot$3.cn.ca
# ssh \
#  -L 15672:10.221.9.$1:15672 \
#  -L 5672:192.168.101.$2:5672 \
#  -L 8906:10.221.9.$1:8906 \
#  -L 9042:192.168.101.$2:9042 \
#  -L 5601:192.168.101.$2:5601 \
#  -L 9001:192.168.101.$2:9001 \
#  -L 8989:192.168.101.$2:8989 \
#  -L 3000:192.168.101.$2:3000 \
#  -N iotvision@iot$3.cn.ca
# 
